// Derived from https://github.com/sethvincent/rewrite-markdown-urls/blob/master/index.js
const assert = require('assert')
const path = require('path')
const isRelativeUrl = require('is-relative-url')

/**
 * @param {String} src – The markdown string with links to rewrite
 * @param {Object} options – options object
 * @param {String} options.baseRemoteUrl – the baseurl of the links. default is an empty string
 * @param {String} options.root – the root directory of the markdown files
 * @example
 **/
module.exports = function rewriteMarkdownUrls (src, options) {
  options = options || {}

  assert.strictEqual(typeof src, 'string', 'src must be a string of markdown')
  assert.strictEqual(typeof options, 'object', 'options must be an object')
  assert.strictEqual(typeof options.root, 'string', 'options.root must the url prefix for the converted links')
  assert.strictEqual(typeof options.baseRemoteUrl, 'string', 'remote rele')

  // Images will not be served locally
  const imagesMatchRegex = /!\[([^\]]*)]\((.+?)(\))/g
  const imagesPathsFixed = src.replace(imagesMatchRegex, function (str, text, link) {
    if (!isRelativeUrl(link)) return str
    return `![${text}](${options.baseRemoteUrl}/${link})`
  })

  // But internal links will resolve
  const mdMatchRegex = /([^!])\[([^\]]*)]\((.+?)(\))/g

  const linkReplaced = imagesPathsFixed.replace(mdMatchRegex, function (str, prefix, text, link) {
    if (!isRelativeUrl(link)) return str

    const { root } = options
    link = path.normalize(`${root}/${link}`)

    return `${prefix}[${text}](${link})`
  })

  return linkReplaced
}
