const rewrite = require('./rewriteLinks')
const md = require('markdown-it')()
const { https } = require('follow-redirects')
const assert = require('assert')

class mdMirror {
  static log (...args) {
    console.log('[md-mirror] ', ...args)
  }

  static async html (url, options) {
    mdMirror._checkRequiredOptions(options)
    const { baseRemoteUrl, baseLocalUrl } = options

    // Test if supplied url is in the form : [baseUrl]/[repoName]/[branchName]/[whatever]
    // If test pass url is saved in two vars, one containing repo + branch names, the other the rest of the query string
    const urlRegex = new RegExp(`^${baseLocalUrl}/([^/]+/[^/]+)/(.+)?$`)
    if (!urlRegex.test(url)) {
      throw new Error(`Url should be formed as: [baseUrl]/[repoName]/[branchName]/[whatever]. Supplied baseUrl : ${baseLocalUrl}`)
    }
    const matches = url.match(urlRegex)

    // Paste the query string at the end of the base remote url
    const documentRemoteUrl = `${baseRemoteUrl}/${matches[1]}/${matches[2] || ''}`

    // Check for cached version
    const cachedVersion = mdMirror._checkCache(url, options)
    if (cachedVersion) {
      mdMirror.log('Cached version found. Rendering it')
      return cachedVersion
    }

    // Fetch the document
    const doc = await mdMirror._fetch(documentRemoteUrl)

    mdMirror.log('Document fetched')
    // noinspection UnnecessaryLocalVariableJS
    const documentPath = matches[2].split('/')
    documentPath.pop()

    const processedDoc = await mdMirror._process(doc, {
      baseRemoteUrl: `${baseRemoteUrl}/${matches[1]}/${documentPath.join('/')}`,
      root: `${baseLocalUrl}/${matches[1]}`
    })

    return processedDoc
  }

  static async render (url, options) {
    mdMirror._checkRequiredOptions(options, 'viewManager')
    return mdMirror.html(url, options).then(options.viewManager)
  }

  static _checkRequiredOptions (options, ...extra) {
    assert.strictEqual(typeof options.baseLocalUrl, 'string', 'options.baseLocalUrl must be a string (can be empty)')

    extra.forEach(requiredOption => {
      if (!Object.hasOwnProperty.call(options, requiredOption) || !options[requiredOption]) {
        throw new Error(`Missing required option ${requiredOption}`)
      }
    })

    if (typeof options.cacheManager !== 'function') {
      mdMirror.log('No cache manager supplied')
    }

    if (typeof options.viewManager !== 'function') {
      mdMirror.log('No view manager supplied')
    }
  }

  static _checkCache (url, options) {
    return options.cacheManager ? options.cacheManager(url) : false
  }

  // Rewrite links & convert to HTML
  static async _process (data, options) {
    mdMirror.log('Processing document')
    const linksRewrittenDocument = await rewrite(data, options)

    // noinspection UnnecessaryLocalVariableJS
    const htmlDocument = await md.render(linksRewrittenDocument)

    return htmlDocument
  }

  static async _fetch (url) {
    return new Promise((resolve, reject) => {
      mdMirror.log(`Fetching ${url}`)
      const req = https.request(url, (res) => {
        res.setEncoding('utf8')
        let responseBody = ''

        res.on('data', (chunk) => {
          responseBody += chunk
        })

        res.on('end', () => {
          resolve(responseBody)
        })
      })

      req.on('error', reject)
      req.end()
    })
  }

  constructor (options) {
    this.options = options
  }

  async html (url) {
    return mdMirror.html(url, this.options)
  }

  async render (url) {
    return mdMirror.render(url, this.options)
  }
}

module.exports = mdMirror
