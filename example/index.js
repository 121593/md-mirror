/*
 * Basic example app
 * Navigation in meshtastic/Meshtastic-esp32
 */
const { createServer } = require('http')
const { renderFile } = require('ejs')
const { join } = require('path')

const MdMirror = require('../src')

const renderTemplate = (res, data = {}) => {
  renderFile(join(__dirname, '/index.ejs'), data, function (err, html) {
    res.writeHead(err ? 400 : 200, { 'Content-Type': 'text/html' })
    res.write(html || 'Error rendering ejs')
    res.end()
  })
}

const localUrlToVisit = 'http://localhost:8080/git/Meshtastic-esp32/master/README.md'

const server = createServer(function (req, res) {
  // Navigation in fixed repo
  if (!/^\/git\/Meshtastic-esp32\/.+/.test(req.url)) {
    res.writeHead(302, {
      Location: localUrlToVisit
    })
    res.end()
  }

  const mm = new MdMirror({
    baseLocalUrl: '/git',
    baseRemoteUrl: 'https://raw.githubusercontent.com/meshtastic',
    viewManager: content => renderTemplate(res, { content })
  })

  mm.render(req.url)
})

server.listen(8080)
