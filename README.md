# md-mirror

Fetch and mirror remote md files


Primary goal is to mirror files on GitHub but it should works with other services

[![Build status](https://gitlab.com/121593/md-mirror/badges/master/pipeline.svg)](https://gitlab.com/121593/md-mirror/pipelines)
[![npm version](https://badge.fury.io/js/md-mirror.svg)](https://badge.fury.io/js/md-mirror)
[![Dependencies](https://david-dm.org/121593/md-mirror.svg)](https://david-dm.org/121593/md-mirror.svg)
[![ISC license](http://img.shields.io/badge/license-ISC-brightgreen.svg)](http://opensource.org/licenses/ISC)

## Installation
`npm i md-mirror`

## Usage
Methods can be used statically, or through an instance holding options. One of them return html content, this other call the supplied render method with that html content

### Statically
```ecmascript 6
// Logging html from a remote md file
const MdMirror = require('md-mirror')

MdMirror.html('/Meshtastic-esp32/master/README.md', {
  baseLocalUrl: '',
  baseRemoteUrl: 'https://raw.githubusercontent.com/meshtastic',
}).then(console.log)
```

### With an instance
```ecmascript 6
// Here we are rendering html from https://raw.githubusercontent.com/username repositories at the website root
// local url in the form : /reponame/branchname/whatever
const MdMirror = require('md-mirror')

app.get('/*', (req, res) => {
  const mm = new MdMirror({
    baseLocalUrl: '',
    baseRemoteUrl: 'https://raw.githubusercontent.com/username',
    viewManager: content => res.render('someview', { content })
  })

  return mm.render(req.url)
})
```
### Caching
Caching have not been tried yet.
@todo


## Example
An example application is included. Run it with : 

`npm run dev` 

and open your browser at [localhost:8080](http://localhost:8080)

## Contributing
Issue reports, pull requests, suggestions and comments are very welcome, don't hesitate !

Commits with [commitizen](https://github.com/commitizen/cz-cli). Style : [standard](https://standardjs.com/)

## License

[ISC](https://choosealicense.com/licenses/isc/)

Copyright (c) 2020, [121593](https://github.com/121593)
